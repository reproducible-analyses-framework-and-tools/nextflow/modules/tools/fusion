#!/usr/bin/env nextflow


// Pre-processing

include { manifest_to_raw_fqs } from '../preproc/preproc.nf'
include { raw_fqs_to_procd_fqs } from '../preproc/preproc.nf'

// Fusion Callers

include { arriba } from '../arriba/arriba.nf'
//Arriba-specific STAR processes to keep version compatibility.
include { arriba_star_index } from '../arriba/arriba.nf'
include { arriba_star_map } from '../arriba/arriba.nf'

include { starfusion } from '../starfusion/starfusion.nf'
include { jstarfusion } from '../starfusion/starfusion.nf'

include { fusioncatcher } from '../fusioncatcher/fusioncatcher.nf'

// Cleaning
include { clean_work_files as clean_starfusion_bams } from '../utilities/utilities.nf'
include { clean_work_files as clean_starfusion_bams2 } from '../utilities/utilities.nf'
include { clean_work_files as clean_arriba_bams } from '../utilities/utilities.nf'

// Misc.
include { star_index } from '../star/star.nf'
include { star_map } from '../star/star.nf'


jstarfusion_star_parameters = "--outReadsUnmapped None \
                               --twopassMode Basic \
                               --outSAMstrandField intronMotif \
                               --outSAMunmapped Within \
                               --chimSegmentMin 12 \
                               --chimJunctionOverhangMin 8 \
                               --chimOutJunctionFormat 1 \
                               --alignSJDBoverhangMin 10 \
                               --alignMatesGapMax 100000 \
                               --alignIntronMax 100000 \
                               --alignSJstitchMismatchNmax 5 -1 5 5 \
                               --chimMultimapScoreRange 3 \
                               --chimScoreJunctionNonGTAG -4 \
                               --chimMultimapNmax 20 \
                               --chimNonchimScoreDropMin 10 \
                               --peOverlapNbasesMin 12 \
                               --peOverlapMMp 0.1 \
                               --alignInsertionFlush Right \
                               --alignSplicedMateMapLminOverLmate 0 \
                               --alignSplicedMateMapLmin 30"
arriba_star_parameters = "--outSAMtype BAM SortedByCoordinate \
                          --outFilterMultimapNmax 50 \
                          --peOverlapNbasesMin 10 \
                          --alignSplicedMateMapLminOverLmate 0.5 \
                          --alignSJstitchMismatchNmax 5 -1 5 5 \
                          --chimSegmentMin 10 \
                          --chimOutType WithinBAM HardClip \
                          --chimJunctionOverhangMin 10 \
                          --chimScoreDropMax 30 \
                          --chimScoreJunctionNonGTAG 0 \
                          --chimScoreSeparation 1 \
                          --chimSegmentReadGapMax 3 \
                          --chimMultimapNmax 50"

workflow manifest_to_fusions {
// take:
//   fq_trim_tool - FASTQ Processing Tool
//   fq_trim_tool - FASTQ Processing Tool Parameters
//   fusion_tool - Fusion Detection Tool
//   fusion_tool_parameters - Fusion Tool Parameters
//   fusion_ref - Fusion Reference Directory
//   dna_ref - Reference Genome FASTA
//   gtf - Gene Annotation File
//   manifest - RAFT Manifest
//
// emit:
//   fusions - Fusions
//   starfusion_fusions - STARFusion Fusions
//   jstarfusion_fusions - Junction-based STARFusion Fusions
//   arriba_fusions - Arriba Fusions

// require:
//   params.fusion$manifest_to_fusions$fq_trim_tool
//   params.fusion$manifest_to_fusions$fq_trim_tool_parameters
//   params.fusion$manifest_to_fusions$fusion_tool
//   params.fusion$manifest_to_fusions$fusion_tool_parameters
//   params.fusion$manifest_to_fusions$fusion_ref
//   params.fusion$manifest_to_fusions$dna_ref
//   params.fusion$manifest_to_fusions$gtf
//   MANIFEST
  take:
    fq_trim_tool
    fq_trim_tool_parameters
    fusion_tool
    fusion_tool_parameters
    fusion_ref
    dna_ref
    gtf
    manifest
  main:
    manifest_to_raw_fqs(
      manifest)
    raw_fqs_to_fusions(
      fq_trim_tool,
      fq_trim_tool_parameters,
      fusion_tool,
      fusion_tool_parameters,
      fusion_ref,
      dna_ref,
      gtf,
      manifest_to_raw_fqs.out.fqs)
  emit:
    fusions = raw_fqs_to_fusions.out.fusions
    starfusion_fusions = raw_fqs_to_fusions.out.starfusion_fusions
    jstarfusion_fusions = raw_fqs_to_fusions.out.starfusion_fusions
    arriba_fusions = raw_fqs_to_fusions.out.arriba_fusions
}


workflow raw_fqs_to_fusions {
// take:
//   fq_trim_tool - FASTQ Processing Tool
//   fq_trim_tool - FASTQ Processing Tool Parameters
//   fusion_tool - Fusion Detection Tool
//   fusion_tool_parameters - Fusion Tool Parameters
//   fusion_ref - Fusion Reference Directory
//   dna_ref - Reference Genome FASTA
//   gtf - Gene Annotation File
//   fqs - Raw FASTQs
//
// emit:
//   fusions - STARFusion Fusions
//   starfusion_fusions - STARFusion Fusions
//   jstarfusion_fusions - Junction-based STARFusion Fusions
//   arriba_fusions - Arriba Fusions

// require:
//   params.fusion$raw_fqs_to_fusions$fq_trim_tool
//   params.fusion$raw_fqs_to_fusions$fq_trim_tool_parameters
//   params.fusion$raw_fqs_to_fusions$fusion_tool
//   params.fusion$raw_fqs_to_fusions$fusion_tool_parameters
//   params.fusion$raw_fqs_to_fusions$fusion_ref
//   params.fusion$raw_fqs_to_fusions$dna_ref
//   params.fusion$raw_fqs_to_fusions$gtf
//   FQS
  take:
    fq_trim_tool
    fq_trim_tool_parameters
    fusion_tool
    fusion_tool_parameters
    fusion_ref
    dna_ref
    gtf
    fqs
  main:
    raw_fqs_to_procd_fqs(
      fqs,
      fq_trim_tool,
      fq_trim_tool_parameters)
    procd_fqs_to_fusions(
      fusion_tool,
      fusion_tool_parameters,
      fusion_ref,
      dna_ref,
      gtf,
      raw_fqs_to_procd_fqs.out.procd_fqs)
  emit:
    fusions = procd_fqs_to_fusions.out.fusions
    starfusion_fusions = procd_fqs_to_fusions.out.starfusion_fusions
    jstarfusion_fusions = procd_fqs_to_fusions.out.starfusion_fusions
    arriba_fusions = procd_fqs_to_fusions.out.arriba_fusions
}


workflow procd_fqs_to_fusions {
// take:
//   fusion_tool - Fusion Detection Tool
//   fusion_tool_parameters - Fusion Tool Parameters
//   fusion_ref - Fusion Reference Directory
//   dna_ref - Reference Genome FASTA
//   gtf - Gene Annotation File
//   procd_fqs - Processed FASTQs
//
// emit:
//   fusions - STARFusion Fusions
//   starfusion_fusions - STARFusion Fusions
//   jstarfusion_fusions - Junction-based STARFusion Fusions
//   arriba_fusions - Arriba Fusions

// require:
//   params.fusion$raw_fqs_to_fusions$fusion_tool
//   params.fusion$raw_fqs_to_fusions$fusion_tool_parameters
//   params.fusion$raw_fqs_to_fusions$fusion_ref
//   params.fusion$raw_fqs_to_fusions$dna_ref
//   params.fusion$raw_fqs_to_fusions$gtf
//   PROCD_FQS
  take:
    fusion_tool
    fusion_tool_parameters
    fusion_ref
    dna_ref
    gtf
    procd_fqs
  main:
    multitools = ''
    fusions = ''
    arriba_fusions = ''
    starfusion_fusions = ''
    jstarfusion_fusions = ''
    fusioncatcher_fusions = ''

    if( fusion_tool =~ /,/ ) {
      println "Running multiple fusion callers -- use tool-specific output channels."
      multitools = 'True'
    }

    fusion_tool_parameters = Eval.me(fusion_tool_parameters)
    fusion_ref = Eval.me(fusion_ref)

    if( fusion_tool =~ /fusioncatcher/) {
      fusioncatcher_tool_parameters = fusion_tool_parameters['fusioncatcher'] ? fusion_tool_parameters['fusioncatcher'] : ''
      fusioncatcher_ref = fusion_ref['fusioncatcher'] ? fusion_ref['fusioncatcher'] : ''
      fusioncatcher(
        procd_fqs,
        fusioncatcher_ref,
        fusioncatcher_tool_parameters)

      // Setting output channels
      fusioncatcher.out.fusions
        .set{ fusioncatcher_fusions}
      if( multitools != 'True' ) {
        fusioncatcher.out.fusions
        .set{ fusions }
      }
    }

    if( fusion_tool =~ /jstarfusion/ ){
      jstarfusion_tool_parameters = fusion_tool_parameters['jstarfusion'] ? fusion_tool_parameters['jstarfusion'] : ''
      jstarfusino_star_index_parameters = fusion_tool_parameters['jstarfusion_star_index'] ? fusion_tool_parameters['jstarfusion_star_index'] : ''
      jstarfusino_star_parameters = fusion_tool_parameters['jstarfusion_star'] ? fusion_tool_parameters['jstarfusion_star'] : jstarfusion_star_parameters
      jstarfusion_ref = fusion_ref['jstarfusion'] ? fusion_ref['jstarfusion'] : ''
      star_index(
        dna_ref,
        jstarfusion_star_index_parameters)
      star_map(
        procd_fqs,
        star_index.out.idx_files,
        jstarfusion_star_parameters,
        gtf)
      junctions_to_fusions(
        'jstarfusion',
        jstarfusion_tool_parameters,
        jstarfusion_ref,
        star_map.out.junctions)

      // Setting output channels
      junctions_to_starfusions.out.full_fusions
        .set{ jstarfusion_fusions }
      if( multitools != 'True' ) {
        junctions_to_starfusions.out.full_fusions
        .set{ fusions }
      }

      // Cleaning intermediate files
      star_map.out.alns
        .join(junctions_to_starfusions.out.full_fusions, by: [0, 1, 2])
        .flatten()
        .filter{ it =~ /sam$|bam$/ }
        .set { starfusion_bams_done_signal }
      clean_starfusion_bams(
        starfusion_bams_done_signal)
    }

    if ( fusion_tool =~ /(^|,)starfusion/ ) {
      starfusion_tool_parameters = fusion_tool_parameters['starfusion'] ? fusion_tool_parameters['starfusion'] : ''
      starfusion_ref = fusion_ref['starfusion'] ? fusion_ref['starfusion'] : ''
      starfusion(
        procd_fqs,
        starfusion_ref,
        starfusion_tool_parameters)

      // Setting output channels
      starfusion.out.full_fusions
        .set{ starfusion_fusions }
      if( multitools != 'True' ) {
        starfusion.out.full_fusions
          .set{ fusions }
      }

      // Cleaning intermediate files
      // Note: BAMs are emittied _with_ fusion reports, so no need to check
      // downstream dependencies.
      clean_starfusion_bams2(
        starfusion.out.bams)
    }

    if ( fusion_tool =~ /arriba/ ) {
      arriba_tool_parameters = fusion_tool_parameters['arriba'] ? fusion_tool_parameters['arriba'] : ''
      arriba_star_index_parameters = fusion_tool_parameters['arriba_star_index'] ? fusion_tool_parameters['arriba_star_index'] : ''
      arriba_star_parameters = fusion_tool_parameters['arriba_star'] ? fusion_tool_parameters['arriba_star'] : arriba_star_parameters
      arriba_ref = fusion_ref['arriba'] ? fusion_ref['arriba'] : ''
      arriba_star_index(
        dna_ref,
        arriba_star_index_parameters)
      arriba_star_map(
        procd_fqs,
        arriba_star_index.out.idx_files,
        arriba_star_parameters,
        gtf)
      alns_to_fusions(
        fusion_tool,
        fusion_tool_parameters,
        fusion_ref,
        dna_ref,
        gtf,
        arriba_star_map.out.alns)

      arriba_fusion_to_standard_fusions(
        alns_to_fusions.out.fusions)

      // Setting output channels
      alns_to_fusions.out.fusions
        .set{ arriba_fusions }
      if( multitools != 'True' ) {
        alns_to_fusions.out.arriba_fusions
        .set{ fusions }
      }

      // Cleaning intermediate files
      arriba_star_map.out.alns
        .join(alns_to_fusions.out.arriba_fusions, by: [0, 1, 2])
        .flatten()
        .filter{ it =~ /sam$|bam$/ }
        .set { arriba_bams_done_signal }
      clean_arriba_bams(
        arriba_bams_done_signal)
    }
  emit:
    jstarfusion_fusions
    starfusion_fusions
    coding_effect_fusions = starfusion.out.coding_effect_fusions
    arriba_fusions
    fusioncatcher_fusions
    fusions
}


workflow junctions_to_fusions {
// take:
//   fusion_ref - Trinity CTAT Reference
//   fusion_parameters - STARFusion Parameters
//   juncts - STAR Junctions
//
// emit:
//   coding_effect_fusions - STARFusion '--examine_coding_effect' Outputs (if available)
//   full_fusions - STARFusion Fusions

// require:
//   params.fusion$junctions_to_fusions$fusion_tool
//   params.fusion$junctions_to_fusions$fusion_tool_parameters
//   params.fusion$junctions_to_fusions$fusion_ref
//   JUNCTIONS

  take:
    fusion_tool
    fusion_tool_parameters
    fusion_ref
    juncts
  main:
    coding_effect_fusions = ''
    starfusion_fusions = ''
    fusion_tool_parameters = Eval.me(fusion_tool_parameters)
    fusion_ref = Eval.me(fusion_ref)
    multitools = ''
    if( fusion_tool =~ /,/ ) {
      multitools = 'True'
    }
    if( fusion_tool =~ /jstarfusion/) {
      jstarfusion_parameters = fusion_tool_parameters['jstarfusion'] ? fusion_tool_parameters['jstarfusion'] : ''
      jstarfusion_ref = fusion_ref['jstarfusion'] ? fusion_ref['jstarfusion'] : ''
      jstarfusion(
        juncts,
        jstarfusion_ref,
        jstarfusion_parameters)
    }
  emit:
    coding_effect_fusions = jstarfusion.out.coding_effect_fusions
    full_fusions = jstarfusion.out.full_fusions
}

workflow alns_to_fusions {
// take:
//   fusion_ref - Trinity CTAT Reference
//   fusion_tool_parameters - STARFusion Parameters
//   juncts - STAR Junctions
//
// emit:

// require:
//   params.fusion$alns_to_fusions$fusion_tool
//   params.fusion$alns_to_fusions$fusion_tool_parameters
//   params.fusion$alns_to_fusions$fusion_ref
//   params.fusion$alns_to_fusions$dna_ref
//   params.fusion$alns_to_fusions$gtf
//   ALNS
  take:
    fusion_tool
    fusion_tool_parameters
    fusion_ref
    dna_ref
    gtf
    alns
  main:
    fusions = ''
    arriba_fusions = ''
    fusion_tool_parameters = Eval.me(fusion_tool_parameters)                                        
    fusion_ref = Eval.me(fusion_ref) 
    multitools = ''
    if( fusion_tool =~ /,/ ) {
      multitools = 'True'
    }
    if( fusion_tool =~ /arriba/) {
      arriba_tool_parameters = fusion_tool_parameters['arriba'] ? fusion_tool_parameters['arriba'] : ''
      arriba_ref = fusion_ref['arriba'] ? fusion_ref['arriba'] : ''
      arriba(
        alns,
        arriba_ref,
        dna_ref,
        gtf,
        arriba_tool_parameters)

      // Setting output channels
      arriba.out.fusions
        .set{ arriba_fusions }
      if( multitools != 'True' ) {
        arriba.out.fusions
        .set{ fusions }
      }
    }
  emit:
    fusions = fusions
    arriba_fusions = arriba_fusions
}

process arriba_fusions_to_standard_fusions {
// Standardizes Arriba fusion output to be compatiable with downstream tools.
  tag "${dataset}/${pat_name}/${run}"
  cache 'lenient'

  input:
  tuple val(pat_name), val(run), val(dataset), path(fusion_data)

  output:
  tuple val(pat_name), val(run), val(dataset), path("*strandard.fusions"), emit: standard_fusions

  script:
  """
  """
}



process fusions_to_extracted_reads {
// Extracts fusion-assocaited read identifiers from starfusion outputs.
//
// input:
//   tuple
//     val(pat_name) - Patient Name
//     val(run) -  Run Name
//     val(dataset) - Dataset
//     path(fusion_data) - STARFusion fusions
//
// output:
//   tuple => emit: fusion_read_names
//     val(pat_name) -  Patient Name
//     val(run) -  Run Name
//     val(dataset) -  Dataset
//     path("*fusion_read_names") - Fusion-supporting Read Names

  tag "${dataset}/${pat_name}/${run}"
  cache 'lenient'

  input:
  tuple val(pat_name), val(run), val(dataset), path(fusion_data)

  output:
  tuple val(pat_name), val(run), val(dataset), path("*fusion_read_names"), emit: fusion_read_names

  script:
  """
  JUNCTION_COL_IDX=`sed -n \$'1s/\\\\\t/\\\\\n/gp' ${fusion_data} | grep -nx 'JunctionReads' | cut -d: -f1`
  SPANNING_COL_IDX=`sed -n \$'1s/\\\\\t/\\\\\n/gp' ${fusion_data} | grep -nx 'SpanningFrags' | cut -d: -f1`

  if [[ \${JUNCTION_COL_IDX} ]]; then

    #Grabbing neutral read identifier
    cut -f \$JUNCTION_COL_IDX,\$SPANNING_COL_IDX ${fusion_data} | \
    sed 's/\$/\\n/g' | \
    sed 's/,/\\n/g' | \
    sed 's/\\s/\\n/g' | \
    grep -v '^\$' | \
    cut -f 2 -d '@' > ${dataset}-${pat_name}-${run}.fusion_read_names

    #Grabbing first read identifier
    cut -f \$JUNCTION_COL_IDX,\$SPANNING_COL_IDX ${fusion_data} | \
    sed 's/\$/\\/1\\n/g' | \
    sed 's/,/\\/1\\n/g' | \
    sed 's/\\s/\\/1\\n/g' | \
    grep -v '^\$' | \
    cut -f 2 -d '@' >> ${dataset}-${pat_name}-${run}.fusion_read_names

    #Grabbing second read identifier
    cut -f \$JUNCTION_COL_IDX,\$SPANNING_COL_IDX ${fusion_data} | \
    sed 's/\$/\\/2\\n/g' | \
    sed 's/,/\\/2\\n/g' | \
    sed 's/\\s/\\/2\\n/g' | \
    grep -v '^\$' | \
    cut -f 2 -d '@' >> ${dataset}-${pat_name}-${run}.fusion_read_names
  fi

  READ_ID_COL_IDX=`sed -n \$'1s/\\\\\t/\\\\\n/gp' ${fusion_data} | grep -nx 'read_identifiers' | cut -d: -f1`
  if [[ \${READ_ID_COL_IDX} ]]; then
    #Grabbing neutral read identifier
    cut -f \$JUNCTION_COL_IDX,\$SPANNING_COL_IDX ${fusion_data} | \
    sed 's/\$/\\n/g' | \
    sed 's/,/\\n/g' | \
    sed 's/\\s/\\n/g' | \
    grep -v '^\$' | \
    cut -f 2 -d '@' > ${dataset}-${pat_name}-${run}.fusion_read_names
  fi
  """
}


process starfusion_fusions_to_fusion_txs {
// Extracts fusion-associated read identifiers from starfusion outputs.
//
// input:
//   tuple
//     val(pat_name) - Patient Name
//     val(run) -  Run Name
//     val(dataset) - Dataset
//     path(fusion_data) - STARFusion fusions
//
// output:
//   tuple => emit: fusion_transcripts
//     val(pat_name) -  Patient Name
//     val(run) -  Run Name
//     val(dataset) -  Dataset
//     path("*fusion_transcripts") - Transcripts with Fusions

  tag "${dataset}/${pat_name}/${run}"
  cache 'lenient'

  input:
  tuple val(pat_name), val(run), val(dataset), path(fusion_data)

  output:
  tuple val(pat_name), val(run), val(dataset), path("*fusion_transcripts"), emit: fusion_transcripts

  script:
  """
  cut -f 18,20 *tsv | \
  grep ENS | \
  sed 's/\\t/\\n/g' >> ${dataset}-${pat_name}-${run}.fusion_transcripts
  """
}
